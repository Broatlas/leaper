//chess.h
#include <iostream>
#include "chesspiece.h"
#ifndef CHESS_H_
#define CHESS_H_
#define ALL_CHESS_PIECES 36
#define CHESS_BOARD 8
#define OFFSET_POS 1
#define EMPTY '.'

#define INIT_ROW_1_W 0
#define INIT_ROW_2_W 1
#define INIT_ROW_7_B 6
#define INIT_ROW_8_B 7
using namespace std;

class Chess
{
	char board[CHESS_BOARD][CHESS_BOARD]; // cheange to some constants later
	unsigned short size;
	char turn;
	char sel_piece;

	unsigned short xy;
	unsigned short x_move;
	unsigned short y_move;
	unsigned short amount_pieces;
	ChessPiece pieces[ALL_CHESS_PIECES];
public:
	Chess();
	int l_move();
	int l_postion();
	void print_turn();
	~Chess()
	{
	if (pieces)
		delete[]pieces;
	}

	friend ostream& operator<<(ostream&, Chess&);

};


#endif