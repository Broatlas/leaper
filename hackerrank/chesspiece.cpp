//chespiece.cpp
#include <iostream>
#include "chesspiece.h"

using namespace std;

ostream& operator<<(ostream& os, ChessPiece& p){
	os << "IN CHESS PIECE";
	os << "Side :"; 
	if (p.side == w_turn){ os << " White "; }
	if (p.side == b_turn){ os << " Black "; }
	os << endl;
	if (p.type == PAWN){ os << "Pawn"; }
	if (p.type == ROOK){ os << "Rook"; }
	if (p.type == KNIGHT){ os << "Knight"; }
	if (p.type == BISHOP){ os << "Bishop"; }
	if (p.type == QUEEN){ os << "Queen"; }
	if (p.type == KING){ os << "King"; }
	os << endl;

	os << "Postion: ";
	if (p.x == A){ os << "A"; }
	if (p.x == B){ os << "B"; }
	if (p.x == C){ os << "C"; }
	if (p.x == D){ os << "D"; }
	if (p.x == E){ os << "E"; }
	if (p.x == F){ os << "F"; }
	if (p.x == G){ os << "G"; }
	if (p.x == H){ os << "H"; }
	os << (p.y + 1) << endl;

	return os;
}