//chesspiece.h

#ifndef CHESSPIECE_H_
#define CHESSPIECE_H_

#include <iostream>


#define PAWN 'p'
#define ROOK 'R'
#define KNIGHT 'k'
#define BISHOP 'B'
#define QUEEN 'Q'
#define KING 'K'

#define A 0
#define B 1
#define C 2
#define D 3
#define E 4
#define F 5
#define G 6
#define H 7

#define b_turn 'b'
#define w_turn 'w'
using namespace std;

class ChessPiece{
	char type, side;

	unsigned short x ,y;

	char direction;
public:
	ChessPiece(char t = 0 , char s = 0, short y = 0, short x = 0):x(x),y(y),type(t),side(s) {}
	friend ostream& operator<<(ostream&, ChessPiece&);
	~ChessPiece(){}
};


#endif
