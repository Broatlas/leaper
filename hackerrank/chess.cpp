//chess.cpp
#include <malloc.h>
#include <stdio.h>
#include "chess.h"
//chess.cpp
#include <malloc.h>
#include <stdio.h>
#include "chess.h"
#include "chesspiece.h"
using namespace std;

Chess::Chess(){
	this->turn = w_turn; // white always goes first
	this->sel_piece = 0; // waiting for playter input
	this->xy = 0;
	this->x_move = 0;
	this->y_move = 0;
	this->amount_pieces = 0;
	//initilizat the board and chess pieces.
	for (int i = 0; i < (CHESS_BOARD); i++)
	{
		for (int j = 0; j < (CHESS_BOARD); j++){
			this->board[i][j] = EMPTY;

			if (INIT_ROW_1_W == i){
				if (j == 0 || j == 7){
					this->board[i][j] = ROOK;
					this->pieces[amount_pieces] = ChessPiece(ROOK, w_turn, i, j);
					amount_pieces++;
				}
				if (j == 1 || j == 6){
					this->board[i][j] = KNIGHT;
					this->pieces[amount_pieces] = ChessPiece(KNIGHT, w_turn, i, j);
					amount_pieces++;
				}
				if (j == 2 || j == 5){
					this->board[i][j] = BISHOP;
					this->pieces[amount_pieces] = ChessPiece(BISHOP, w_turn, i, j);
					amount_pieces++;
				}
				if (j == 3){
					this->board[i][j] = KING;
					this->pieces[amount_pieces] = ChessPiece(KING, w_turn, i, j);
					amount_pieces++;
				}
				if (j == 4){
					this->board[i][j] = QUEEN;
					this->pieces[amount_pieces] = ChessPiece(QUEEN, w_turn, i, j);
					amount_pieces++;
				}
			}
			if (INIT_ROW_2_W == i){
				this->board[i][j] = PAWN;
				this->pieces[amount_pieces] = ChessPiece(PAWN, w_turn, i, j);
				amount_pieces++;
			}
			if (INIT_ROW_7_B == i) {
				this->board[i][j] = PAWN;
				this->pieces[amount_pieces] = ChessPiece(PAWN, b_turn, i, j);
				amount_pieces++;
			}
			if (INIT_ROW_8_B == i){
				if (j == 0 || j == 7){
					this->board[i][j] = ROOK;
					this->pieces[amount_pieces] = ChessPiece(ROOK, b_turn, i, j);
					amount_pieces++;
				}
				if (j == 1 || j == 6){
					this->board[i][j] = KNIGHT;
					this->pieces[amount_pieces] = ChessPiece(KNIGHT, b_turn, i, j);
					amount_pieces++;
				}
				if (j == 2 || j == 5){
					this->board[i][j] = BISHOP;
					this->pieces[amount_pieces] = ChessPiece(BISHOP, b_turn, i, j);
					amount_pieces++;
				}
				if (j == 3){
					this->board[i][j] = QUEEN;
					this->pieces[amount_pieces] = ChessPiece(QUEEN, b_turn, i, j);
					amount_pieces++;
				}
				if (j == 4){
					this->board[i][j] = KING;
					this->pieces[amount_pieces] = ChessPiece(KING, b_turn, i, j);
					amount_pieces++;
				}
			}

		}

	}




}


int Chess::l_move(){

	if (this->turn == w_turn){
		printf("Whites move \n");
		this->turn = b_turn; // once move complete oppent turn
		return 1;
	}
	if (this->turn == b_turn){
		printf("Blacks move \n");
		this->turn = w_turn;// once move complete oppent turn
	}

}
int Chess::l_postion(){
	return 0;
}

void Chess::print_turn(){
	if (this->turn == w_turn){
		cout << " IT IS WHITE'S TURN " << endl;
	}
	if (this->turn == b_turn){ 
		cout << "  IT IS BLACK'S TURN " << endl;
	}
}

ostream& operator<<(ostream& os, Chess& board) {
	for (int i = 0; i < (CHESS_BOARD); i++){
		for (int j = 0; j < (CHESS_BOARD); j++){
			os << " " << board.board[i][j] << " ";
		}
		os << endl;
	}
	//redo
	os << "**************************************" << endl;

	for (int i = 0; i < board.amount_pieces; i++) {
		os << board.pieces[i];
	}
	return os;
}


